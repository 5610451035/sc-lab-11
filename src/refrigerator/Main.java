package refrigerator;

public class Main {
	
	public static void main(String[] args){
		
		Refrigerator ref = new Refrigerator(5);
		
		try {
			ref.put("egg");
			ref.put("lemon");
			ref.put("milk");
			ref.put("strawberry");
			ref.put("water");
			System.out.println("Refrigerator have : "+ref.toString());
			
			System.out.println("GET : "+ref.takeOut("egg"));
			System.out.println("Refrigerator have : "+ref.toString());
			
			System.out.println("GET : "+ref.takeOut("orange"));
			System.out.println("Refrigerator have : "+ref.toString());
			
			ref.put("pork");
			ref.put("chicken");
		}
		
		catch (FullException e){
			System.err.println("ERROR : "+e.getMessage());
			System.out.println("Refrigerator have : "+ref.toString());
			
	 	}	 	

		

	}
}
