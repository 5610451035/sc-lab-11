package refrigerator;

import java.util.ArrayList;

public class Refrigerator {
	
	private int size;
	private ArrayList<String> things;
	
	
	Refrigerator(int size){
		this.size = size;
		this.things = new ArrayList<String>();
	}
	
	public void put(String stuff) throws FullException{
		if (things.size()==size){
			throw new FullException("Refrigerator is full!!");
		}
		else{things.add(stuff);}
	}
	
	public String takeOut(String stuff){
		if (things.contains(stuff)){
			things.remove(stuff);
			return stuff;
		}
		return null;
	}
	
	public String toString(){
		String str = "";
		for(String e:things){
			str = str+e+"\t";
		}
		return str;
	}
}
