package refrigerator;

public class FullException extends Exception {
	
	public FullException() {
		
	}
	
	public FullException(String message) {
		super(message);
	}
}
