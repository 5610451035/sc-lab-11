package dataexception;

public class MyClass {
	
	public void methX() throws DataException{
		//..........
		throw new DataException("ERROR : methX");
	}
	
	public void methY() throws FormatException{
		//..........
		throw new FormatException();
	}

}
