package dataexception;

public class DataException extends Exception {
	
	public DataException() {
		
	}
	
	public DataException(String message) {
		super(message);
	}
}
