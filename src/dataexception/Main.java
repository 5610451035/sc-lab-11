package dataexception;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

public class Main {

	public static void main(String[] args){
		try {
			MyClass myclass = new MyClass();
			System.out.println("A");
			
			myclass.methX();
			System.out.println("B");
			
			myclass.methY();
			System.out.println("C");
			
			return;
		}
		
		catch (DataException e) {
			System.out.println("C");
		}
		
		catch (FormatException e) {
			System.out.println("D");
		}
		
		finally {
			System.out.println("E");
		}
			
		System.out.println("F");
		
	}
}
