package hashmap;

public class Main {
	
	public static void main(String[] args){
		
		WordCounter counter = new WordCounter("got got got mm mm mo oo oo 33 33 33 33 3 33 33");
		counter.count();
	
		System.out.println(counter.hasWord("got"));
		System.out.println(counter.hasWord("mm"));
		System.out.println(counter.hasWord("33"));
	}

}
