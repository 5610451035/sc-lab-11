package hashmap;

import java.util.HashMap;

public class WordCounter {
	
	String message;
	HashMap<String,Integer> wordCount ;
	
	WordCounter(String message){
		this.message = message;
		wordCount = new HashMap<String,Integer>();
	}

	public void count(){
		
		String[] str = message.split(" ");
		for (int i = 0; i<str.length; i++) {
            if (wordCount.containsKey(str[i]) == false) {
            	wordCount.put(str[i], 1);
            } 
            else {
            	wordCount.replace(str[i], wordCount.get(str[i]), wordCount.get(str[i])+1);
            }
        }
	}
	
	public int hasWord(String word) {
		return wordCount.get(word);
	}
	
	
}